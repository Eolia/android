package com.example.eviot.android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class TD1_android extends AppCompatActivity {
    private EditText nombre1;
    private EditText nombre2;
    private TextView resultat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_td1_android);


        nombre1 = (EditText) findViewById(R.id.nombre1);
        nombre2 = (EditText) findViewById(R.id.nombre2);
        resultat = (TextView) findViewById(R.id.resultat);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_td1_android, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void computeAdd(View v)
    {
        if (!nombre1.getText().toString().isEmpty() && !nombre2.getText().toString().isEmpty()){
        int nb1;
        int nb2;
        int result;
        String memoire="";
        nb1 = Integer.parseInt(nombre1.getText().toString());
        nb2 = Integer.parseInt(nombre2.getText().toString());
        result=nb1+nb2;
        memoire = memoire + "Addition = "+Integer.toString(result)+"\n";
        result=nb1*nb2;
        memoire = memoire + "Multiplication = "+Integer.toString(result)+"\n";
        result=nb1-nb2;
        memoire = memoire + "Soustraction = "+Integer.toString(result)+"\n";
        if (nb2!=0) {
            result = nb1 / nb2;
            memoire = memoire + "Division = " + Integer.toString(result);
        }
        resultat.setText(memoire);
        }
    }
}
