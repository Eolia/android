package com.example.td4;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        GitHubClient client = retrofit.create(GitHubClient.class);
        Call<List<RepoList>> call = client.UserRepositories("facebook");

call.enqueue(new Callback<List<RepoList>>() {
        @Override
        public void onResponse(Call<List<RepoList>> call, Response<List<RepoList>> response) {
            List<RepoList> list = response.body();
        }
        @Override
        public void onFailure(Call<List<RepoList>> call, Throwable t) {
            Toast.makeText(MainActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
        }
    });
}
}
